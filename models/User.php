<?php

class User {
	
	private $dbh;
	
	public function __construct()	{
		$this->dbh = new PDO("mysql:host=localhost;dbname=db_phphmvc","root","");
	}

	public function getUsers(){				
		$sth = $this->dbh->prepare("SELECT * FROM vw_pessoa");
		$sth->execute();
		$data = array_merge(
		    array("users" => $sth->fetchAll()),
            array("status" => 1)
        );
		return json_encode($data);
	}

    public function validate($user){

        $return = FALSE;

        if(strlen($user->nome) < 3 OR strlen($user->nome) > 50){
            $return .= "O nome deve conter no mínimo 3 e no máximo 50 caracteres.<br>";
        }
        if(strlen($user->sobrenome) < 3 OR strlen($user->sobrenome) > 100){
            $return .= "O sobrenome deve conter no mínimo 3 e no máximo 100 caracteres.<br>";
        }
        if(count($user->grupos) < 2){
            $return .= "O usuário deve pertencer no mínimo a dois grupos.<br>";
        }
        return $return;
    }

    public function add($user){
        $sth = $this->dbh->prepare("INSERT INTO tbl_pessoas(nome, sobrenome) VALUES (?, ?)");
        $sth->execute(array($user->nome, $user->sobrenome));
        $data = array_merge(array("id" => $this->dbh->lastInsertId()),array("status" => 1));
        return json_encode($data);
    }

	public function delete($user){				
		$sth = $this->dbh->prepare("DELETE FROM tbl_pessoas WHERE id=?");
		$sth->execute(array($user->id));
		return json_encode(array("status" => 1));
	}

    public function edit($user){
        $sth = $this->dbh->prepare("UPDATE tbl_pessoas SET nome=?, sobrenome=? WHERE id=?");
        $sth->execute(array($user->nome,$user->sobrenome, $user->id));
        return json_encode(array("status" => 1));
    }
}