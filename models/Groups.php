<?php

class Groups {
	
	private $dbh;
	
	public function __construct()	{
		$this->dbh = new PDO("mysql:host=localhost;dbname=db_phphmvc","root","");
	}

	public function getGroups(){
		$sth = $this->dbh->prepare("SELECT * FROM tbl_grupos");
		$sth->execute();
		$data = array_merge(
		    array("groups" => $sth->fetchAll()),
            array("status" => 1)
        );
		return json_encode($data);
	}

    public function delete($user){
        $sth = $this->dbh->prepare("DELETE FROM tbl_grupo_pessoa WHERE tbl_pessoas_ID=?");
        $sth->execute(array($user->id));
        return json_encode(array("status" => 1));
    }

    public function add($user){
        $sth = $this->dbh->prepare("INSERT INTO tbl_grupo_pessoa(tbl_grupos_ID, tbl_pessoas_ID) VALUES (?, ?)");
        $sth->execute(array($user->grupo_ID, $user->id));
        $data = array_merge(array("id" => $this->dbh->lastInsertId()),array("status" => 1));
        return json_encode($data);
    }

}