-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.7.19 - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.4.0.5186
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para db_phphmvc
DROP DATABASE IF EXISTS `db_phphmvc`;
CREATE DATABASE IF NOT EXISTS `db_phphmvc` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `db_phphmvc`;

-- Copiando estrutura para tabela db_phphmvc.tbl_grupos
DROP TABLE IF EXISTS `tbl_grupos`;
CREATE TABLE IF NOT EXISTS `tbl_grupos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela db_phphmvc.tbl_grupos: ~5 rows (aproximadamente)
DELETE FROM `tbl_grupos`;
/*!40000 ALTER TABLE `tbl_grupos` DISABLE KEYS */;
INSERT INTO `tbl_grupos` (`id`, `nome`) VALUES
	(1, 'Grupo 1');
INSERT INTO `tbl_grupos` (`id`, `nome`) VALUES
	(2, 'Grupo 2');
INSERT INTO `tbl_grupos` (`id`, `nome`) VALUES
	(3, 'Grupo 3');
INSERT INTO `tbl_grupos` (`id`, `nome`) VALUES
	(4, 'Grupo 4');
INSERT INTO `tbl_grupos` (`id`, `nome`) VALUES
	(5, 'Grupo 5');
/*!40000 ALTER TABLE `tbl_grupos` ENABLE KEYS */;

-- Copiando estrutura para tabela db_phphmvc.tbl_grupo_pessoa
DROP TABLE IF EXISTS `tbl_grupo_pessoa`;
CREATE TABLE IF NOT EXISTS `tbl_grupo_pessoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_grupos_ID` int(11) NOT NULL,
  `tbl_pessoas_ID` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_grupos_ID` (`tbl_grupos_ID`),
  KEY `fk_tbl_pessoas_ID` (`tbl_pessoas_ID`),
  CONSTRAINT `fk_tbl_grupos_ID` FOREIGN KEY (`tbl_grupos_ID`) REFERENCES `tbl_grupos` (`id`),
  CONSTRAINT `fk_tbl_pessoas_ID` FOREIGN KEY (`tbl_pessoas_ID`) REFERENCES `tbl_pessoas` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela db_phphmvc.tbl_grupo_pessoa: ~10 rows (aproximadamente)
DELETE FROM `tbl_grupo_pessoa`;
/*!40000 ALTER TABLE `tbl_grupo_pessoa` DISABLE KEYS */;
INSERT INTO `tbl_grupo_pessoa` (`id`, `tbl_grupos_ID`, `tbl_pessoas_ID`) VALUES
	(8, 4, 3);
INSERT INTO `tbl_grupo_pessoa` (`id`, `tbl_grupos_ID`, `tbl_pessoas_ID`) VALUES
	(13, 1, 3);
INSERT INTO `tbl_grupo_pessoa` (`id`, `tbl_grupos_ID`, `tbl_pessoas_ID`) VALUES
	(16, 5, 3);
INSERT INTO `tbl_grupo_pessoa` (`id`, `tbl_grupos_ID`, `tbl_pessoas_ID`) VALUES
	(76, 2, 10);
INSERT INTO `tbl_grupo_pessoa` (`id`, `tbl_grupos_ID`, `tbl_pessoas_ID`) VALUES
	(77, 3, 10);
INSERT INTO `tbl_grupo_pessoa` (`id`, `tbl_grupos_ID`, `tbl_pessoas_ID`) VALUES
	(78, 1, 11);
INSERT INTO `tbl_grupo_pessoa` (`id`, `tbl_grupos_ID`, `tbl_pessoas_ID`) VALUES
	(79, 4, 11);
INSERT INTO `tbl_grupo_pessoa` (`id`, `tbl_grupos_ID`, `tbl_pessoas_ID`) VALUES
	(80, 1, 1);
INSERT INTO `tbl_grupo_pessoa` (`id`, `tbl_grupos_ID`, `tbl_pessoas_ID`) VALUES
	(81, 3, 1);
INSERT INTO `tbl_grupo_pessoa` (`id`, `tbl_grupos_ID`, `tbl_pessoas_ID`) VALUES
	(82, 5, 1);
/*!40000 ALTER TABLE `tbl_grupo_pessoa` ENABLE KEYS */;

-- Copiando estrutura para tabela db_phphmvc.tbl_pessoas
DROP TABLE IF EXISTS `tbl_pessoas`;
CREATE TABLE IF NOT EXISTS `tbl_pessoas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `sobrenome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela db_phphmvc.tbl_pessoas: ~4 rows (aproximadamente)
DELETE FROM `tbl_pessoas`;
/*!40000 ALTER TABLE `tbl_pessoas` DISABLE KEYS */;
INSERT INTO `tbl_pessoas` (`id`, `nome`, `sobrenome`) VALUES
	(1, 'John', 'Doe Souza');
INSERT INTO `tbl_pessoas` (`id`, `nome`, `sobrenome`) VALUES
	(3, 'Tom', 'Clancy\'s');
INSERT INTO `tbl_pessoas` (`id`, `nome`, `sobrenome`) VALUES
	(10, 'Jane', 'Bar');
INSERT INTO `tbl_pessoas` (`id`, `nome`, `sobrenome`) VALUES
	(11, 'Maria', 'Silva');
/*!40000 ALTER TABLE `tbl_pessoas` ENABLE KEYS */;

-- Copiando estrutura para view db_phphmvc.vw_grupo_pessoa
DROP VIEW IF EXISTS `vw_grupo_pessoa`;
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_grupo_pessoa` (
	`id` INT(11) NOT NULL,
	`nome` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`sobrenome` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`grupo_ID` INT(11) NOT NULL,
	`grupoNome` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Copiando estrutura para view db_phphmvc.vw_pessoa
DROP VIEW IF EXISTS `vw_pessoa`;
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_pessoa` (
	`id` INT(11) NOT NULL,
	`nome` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`sobrenome` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`grupos` TEXT NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Copiando estrutura para view db_phphmvc.vw_grupo_pessoa
DROP VIEW IF EXISTS `vw_grupo_pessoa`;
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_grupo_pessoa`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_grupo_pessoa` AS select `p`.`id` AS `id`,`p`.`nome` AS `nome`,`p`.`sobrenome` AS `sobrenome`,`g`.`id` AS `grupo_ID`,`g`.`nome` AS `grupoNome` from ((`db_phphmvc`.`tbl_pessoas` `p` join (select `db_phphmvc`.`tbl_grupo_pessoa`.`id` AS `id`,`db_phphmvc`.`tbl_grupo_pessoa`.`tbl_grupos_ID` AS `tbl_grupos_ID`,`db_phphmvc`.`tbl_grupo_pessoa`.`tbl_pessoas_ID` AS `tbl_pessoas_ID` from `db_phphmvc`.`tbl_grupo_pessoa` order by `db_phphmvc`.`tbl_grupo_pessoa`.`id` desc) `gp` on((`p`.`id` = `gp`.`tbl_pessoas_ID`))) join (select `db_phphmvc`.`tbl_grupos`.`id` AS `id`,`db_phphmvc`.`tbl_grupos`.`nome` AS `nome` from `db_phphmvc`.`tbl_grupos` order by `db_phphmvc`.`tbl_grupos`.`id` desc) `g` on((`g`.`id` = `gp`.`tbl_grupos_ID`))) order by `p`.`id`,`g`.`id`;

-- Copiando estrutura para view db_phphmvc.vw_pessoa
DROP VIEW IF EXISTS `vw_pessoa`;
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_pessoa`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_pessoa` AS select `p`.`id` AS `id`,`p`.`nome` AS `nome`,`p`.`sobrenome` AS `sobrenome`,group_concat(concat_ws('||',`g`.`grupo_ID`,`g`.`grupoNome`) separator ',') AS `grupos` from (`db_phphmvc`.`tbl_pessoas` `p` left join `db_phphmvc`.`vw_grupo_pessoa` `g` on((`p`.`id` = `g`.`id`))) group by `p`.`id`;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
