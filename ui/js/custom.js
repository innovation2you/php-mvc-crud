$(function() {
	$(document).on("click", "a#user_list", function(){
		getUserList(this);
	});
	$(document).on("click", "a#create_user_form", function(){
		getCreateForm(this);
	});
    $(document).on("click", "button#add_user", function(e){
        e.preventDefault();
    	addUser(this);
    });
    $(document).on("click", "button#edit_user", function(e){
    	e.preventDefault();
    	editUser(this);
    });
	$(document).on("click", "a.delete_confirm", function(){
		deleteConfirmation(this);
	});
	$(document).on("click", "button.delete", function(){
		deleteUser(this);
	});
});

$(document).ready(function() {
    getUserList();
});

function deleteConfirmation(element) {	
	$("#delete_confirm_modal").modal("show");
	$("#delete_confirm_modal input#user_id").val($(element).attr('user_id'));
}

function deleteUser(element) {	
	
	var User = new Object();
	User.id = $("#delete_confirm_modal input#user_id").val();
	
	var userJson = JSON.stringify(User);

    $.ajax({
        type: "POST",
        url: '/Controller.php',
        data: {
            action: 'delete_user',
            user: userJson
        },
        //async: false,
        success: function (result,status,xhr) {
            var data = JSON.parse(result);
            if(data.status === 0){
                var mensagens = "<h3>Ocorreu o seguinte(s) erros:!</h3>";
                mensagens += data.mensagem + "<br>";
                $('#alerta_danger').show();
                $('#alerta_success').hide();
                $('.alertas_mensagem').html(mensagens);
            } else {
                $("#delete_confirm_modal").modal("hide");
                var mensagens = "<h3>Usuário excluido com sucesso!</h3>";
                $('#alerta_danger').hide();
                $('#alerta_success').show();
                $('.alertas_mensagem').html(mensagens);
                getUserList(element);
            }
        },
        error: function (xhr,status,error) {
            alert("Ocorreu algum erro!")
        },
        complete: function (xhr,status,error) {
            $('#loading').hide();
        }
    });
}

function getUserList(element) {

    $('#loading').show();

    $.post('Controller.php',
        {
            action: 'get_users'
        },
        function(data, textStatus) {
            renderUserList(data.users);
            $('#loading').hide();
        },
        "json"
    );
}

function getGroupList() {
    return $.ajax({
        type: "POST",
        url: '/Controller.php',
        data: {
            action: 'get_groups'
        },
        async: false,
        error: function() {
            alert("Error occured")
        }
    });
}

function renderUserList(jsonData) {
	
	var table = '<table width="600" cellpadding="5" class="table table-hover table-bordered"><thead><tr><th scope="col">Nome</th><th scope="col">Sobrenome</th><th scope="col">Grupos</th><th scope="col">Ações</th></tr></thead><tbody>';

	$.each( jsonData, function( index, user){

        var grupos = user.grupos.split(",");
        $.each(grupos, function(index, grupo){
            var item = grupo.split("||");
            if(index === 0){
                gruponomes = item[1];
                gruposid = item[0];
            } else {
                gruponomes += ", " + item[1];
                gruposid += ", " + item[0];
            }
        });

		table += '<tr id="user_'+user.id+'">';
		table += '<td user_id="'+user.id+'" user_nome="'+user.nome+'">'+user.nome+'</td>';
		table += '<td user_sobrenome="'+user.sobrenome+'">'+user.sobrenome+'</td>';
		table += '<td user_grupos="'+gruposid+'">'+gruponomes+'</td>';
        table += '<td><a href="javascript:void(0);" user_id="'+user.id+'" class="delete_confirm btn btn-danger"><i class="icon-remove icon-white"></i></a>';
        table += '&nbsp;&nbsp;<a href="javascript:getEditForm('+user.id+');" class="edit_user btn btn-success"><i class="icon-pencil icon-white"></i></a></td>';
		table += '</tr>';
    });
	
	table += '</tbody></table>';
	
	$('div#content').html(table);
}

function addUser(element) {

    $('#loading').show();

    var row = $("#form_create_user");
    var User = new Object();
    var checkedGrupos = [];
    User.nome = row.find('#nome').val();
    User.sobrenome = row.find('#sobrenome').val();
    $("#form_create_user input:checkbox").each(function () {
        if($(this).is(":checked")){
            checkedGrupos.push($(this).val());
        }
    });
    User.grupos = checkedGrupos;
    var userJson = JSON.stringify(User);

    if(
        User.nome.length < 3 || User.nome.length > 50 ||
        User.sobrenome.length < 3 || User.sobrenome.length > 100 ||
        User.grupos.length <= 1
    ){
        var mensagens = "<h3>Ocorreram os seguintes erros:</h3>";

        if(User.nome.length < 3 || User.nome.length > 50){
            mensagens += "- O nome deve conter no mínimo 3 e no máximo 50 caracteres<br>";
        }
        if(User.sobrenome.length < 3 || User.sobrenome.length > 100){
            mensagens += "- O sobrenome deve conter no mínimo 3 e no máximo 100 caracteres<br>";
        }
        if(User.grupos.length <= 1){
            mensagens += "- O usuário precisa estar em no mínimo 2 grupos<br>";
        }

        $('#alerta_success').hide();
        $('#alerta_danger').show();
        $('.alertas_mensagem').html(mensagens);

        $('#loading').hide();

    } else {


        $.ajax({
            type: "POST",
            url: '/Controller.php',
            data: {
                action: 'add_user',
                user: userJson
            },
            //async: false,
            success: function (result,status,xhr) {
                var data = JSON.parse(result);
                if(data.status === 0){
                    var mensagens = "<h3>Ocorreu o seguinte(s) erros:!</h3>";
                    mensagens += data.mensagem + "<br>";
                    $('#alerta_danger').show();
                    $('#alerta_success').hide();
                    $('.alertas_mensagem').html(mensagens);
                } else {
                    var mensagens = "<h3>Usuário cadastrado com sucesso!</h3>";
                    $('#alerta_danger').hide();
                    $('#alerta_success').show();
                    $('.alertas_mensagem').html(mensagens);
                    getUserList(element);
                }
            },
            error: function (xhr,status,error) {
                alert("Ocorreu algum erro!")
            },
            complete: function (xhr,status,error) {
                $('#loading').hide();
            }
        });

    }

}

function editUser(element) {

    $('#loading').show();

    var User = new Object();
    var checkedGrupos = [];
    User.id = $('input#id_user').val();
    User.nome = $('input#nome').val();
    User.sobrenome = $('input#sobrenome').val();
    $('input.form-check-input').each(function () {
    	if($(this).is(":checked")){
            checkedGrupos.push($(this).val());
		}
    });
    User.grupos = checkedGrupos;
    var userJson = JSON.stringify(User);

    if(
        User.nome.length < 3 || User.nome.length > 50 ||
        User.sobrenome.length < 3 || User.sobrenome.length > 100 ||
        User.grupos.length <= 1
	){
    	var mensagens = "<h3>Ocorreram os seguintes erros:</h3>";

        if(User.nome.length < 3 || User.nome.length > 50){
            mensagens += "- O nome deve conter no mínimo 3 e no máximo 50 caracteres<br>";
        }
        if(User.sobrenome.length < 3 || User.sobrenome.length > 100){
            mensagens += "- O sobrenome deve conter no mínimo 3 e no máximo 100 caracteres<br>";
        }
        if(User.grupos.length <= 1){
            mensagens += "- O usuário precisa estar em no mínimo 2 grupos<br>";
        }

        $('#alerta_success').hide();
        $('#alerta_danger').show();
        $('.alertas_mensagem').html(mensagens);

        $('#loading').hide();

	} else {

        $.ajax({
            type: "POST",
            url: '/Controller.php',
            data: {
                action: 'edit_user',
                user: userJson
            },
            //async: false,
            success: function (result,status,xhr) {
                var data = JSON.parse(result);
                if(data.status === 0){
                    var mensagens = "<h3>Ocorreu o seguinte(s) erros:</h3>";
                    mensagens += data.mensagem + "<br>";
                    $('#alerta_danger').show();
                    $('#alerta_success').hide();
                    $('.alertas_mensagem').html(mensagens);
                } else {
                    var mensagens = "<h3>Usuário alterado com sucesso!</h3>";
                    $('#alerta_danger').hide();
                    $('#alerta_success').show();
                    $('.alertas_mensagem').html(mensagens);
                    getUserList(element);
                }

            },
            error: function (xhr,status,error) {
                alert("Ocorreu algum erro!")
            },
            complete: function (xhr,status,error) {
                $('#loading').hide();
            }
        });

    }

}

function getCreateForm(element) {

    var form = '<div id="form_create_user">';

    form +=	'<div class="form-check col-xs-12">';
    form +=	'<div class="pull-left">Nome:</div><br>';
    form +=	'<input type="text" id="nome" name="nome" value="" class="input-xlarge" />';
    form +=	'</div>';

    form +=	'<div class="form-check col-xs-12">';
    form +=	'<div class="pull-left">Sobrenome:</div><br>';
    form +=	'<input type="text" id="sobrenome" name="sobrenome" value="" class="input-xlarge" />';
    form +=	'</div>';

    form +=	'<div class="col-xs-12">';
    form +=	'<div class="pull-left">Grupos:</div><br>';
    form +=	'<div class="form-check-label pull-left">* Selecione no mínimo 2 itens.</div><br>';
    form +=	'</div>';

    var ajaxObjGroups = getGroupList();
    var ajaxResponseGroups = ajaxObjGroups.responseText;
    var objGroups = JSON.parse(ajaxResponseGroups);
    $.each(objGroups.groups, function(index, grupo){
        form +=	'<div class="form-check col-xs-12">';
        form +=	'<input type="checkbox" name="grupos[]" value="'+grupo.id+'" class="form-check-input pull-left ads_Checkbox" />';
        form +=	'<div class="form-check-label pull-left">&nbsp;&nbsp;&nbsp;'+grupo.nome+'</div><br>';
        form +=	'</div>';
    });

    form +=	'<br/><div class="control-group">';
    form +=	'<div class="">';
    form +=	'<button type="button" id="add_user" class="btn btn-primary"><i class="icon-ok icon-white"></i> Adicionar</button>';
    form +=	'</div>';
    form +=	'</div></div>';

		if($("#form_create_user").is(":visible")){
            $("#form_create_user").remove();
		} else {
            $('div#content').prepend(form)
		}
}

function getEditForm(element) {

	var row = $("#user_"+element+"");
    var User = new Object();
    User.id = element;
    User.nome = row.find('[user_nome]').text();
    User.sobrenome = row.find('[user_sobrenome]').text();
    User.grupos = replaceAll(row.find('[user_grupos]').attr('user_grupos'),' ','');

    var form = '<div id="form_create_user">';

    form +=	'<div class="form-check col-xs-12">';
    form +=	'<div class="pull-left">Nome:</div><br>';
    form +=	'<input type="text" id="nome" name="nome" value="'+User.nome+'" class="input-xlarge" />';
    form +=	'</div>';

    form +=	'<div class="form-check col-xs-12">';
    form +=	'<div class="pull-left">Sobrenome:</div><br>';
    form +=	'<input type="text" id="sobrenome" name="sobrenome" value="'+User.sobrenome+'" class="input-xlarge" />';
    form +=	'</div>';

    form +=	'<div class="col-xs-12">';
    form +=	'<div class="pull-left">Grupos:</div><br>';
    form +=	'<div class="form-check-label pull-left">* Selecione no mínimo 2 itens.</div><br>';
    form +=	'</div>';

    var ajaxObjGroups = getGroupList();
    var ajaxResponseGroups = ajaxObjGroups.responseText;
    var objGroups = JSON.parse(ajaxResponseGroups);
    var gruposAtuais = User.grupos.split(",");
    $.each(objGroups.groups, function(index, grupo){
    	var checked = (gruposAtuais.indexOf(grupo.id) !== -1) ? 'checked' : '';
        form +=	'<div class="form-check col-xs-12">';
        form +=	'<input type="checkbox" name="grupos[]" value="'+grupo.id+'" class="form-check-input pull-left ads_Checkbox" ' + checked + ' />';
        form +=	'<div class="form-check-label pull-left">&nbsp;&nbsp;&nbsp;'+grupo.nome+'</div><br>';
        form +=	'</div>';
    });

    form +=	'<br/><div class="control-group">';
    form +=	'<div class="">';
    form +=	'<input type="hidden" id="id_user" name="id" value="'+element+'">';
    form +=	'<button type="button" id="edit_user" class="btn btn-primary"><i class="icon-ok icon-white"></i> Alterar</button>';
    form +=	'</div>';
    form +=	'</div></div>';

    if($("#form_create_user").is(":visible")){
        $("#form_create_user").remove();
    } else {
        $('div#content').prepend(form)
    }
}

/* Define function for escaping user input to be treated as
   a literal string within a regular expression */
function escapeRegExp(string){
    return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

/* Define functin to find and replace specified term with replacement string */
function replaceAll(str, term, replacement) {
    return str.replace(new RegExp(escapeRegExp(term), 'g'), replacement);
}

//ajax.get('/test.php', {foo: 'bar'}, function(responseText) { alert(responseText); });
//ajax.get('/test.php', {foo: 'bar'}, function() {});

var ajax = {};
ajax.x = function () {
    if (typeof XMLHttpRequest !== 'undefined') {
        return new XMLHttpRequest();
    }
    var versions = [
        "MSXML2.XmlHttp.6.0",
        "MSXML2.XmlHttp.5.0",
        "MSXML2.XmlHttp.4.0",
        "MSXML2.XmlHttp.3.0",
        "MSXML2.XmlHttp.2.0",
        "Microsoft.XmlHttp"
    ];

    var xhr;
    for (var i = 0; i < versions.length; i++) {
        try {
            xhr = new ActiveXObject(versions[i]);
            break;
        } catch (e) {
        }
    }
    return xhr;
};
ajax.send = function (url, callback, method, data, async) {
    if (async === undefined) {
        async = true;
    }
    var x = ajax.x();
    x.open(method, url, async);
    x.onreadystatechange = function () {
        if (x.readyState == 4) {
            callback(x.responseText)
        }
    };
    if (method == 'POST') {
        x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    }
    x.send(data)
};
ajax.get = function (url, data, callback, async) {
    var query = [];
    for (var key in data) {
        query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
    }
    ajax.send(url + (query.length ? '?' + query.join('&') : ''), callback, 'GET', null, async)
};
ajax.post = function (url, data, callback, async) {
    var query = [];
    for (var key in data) {
        query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
    }
    ajax.send(url, callback, 'POST', query.join('&'), async)
};

var teste = ajax.post('/Controller.php',{action: 'get_groups'},function (data) {
    console.log(data);
    return data;
}, false);
console.log(teste);