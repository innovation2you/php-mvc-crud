<?php

function __autoload($className){
	include_once("models/$className.php");	
}

$users = new User();
$groups = new Groups();

if(!isset($_POST['action'])) {
	print json_encode(array("status" => 0));
	return;
}

switch($_POST['action']) {

    case 'get_groups':
        print $groups->getGroups();
    break;

    case 'get_users':
        print $users->getUsers();
    break;

    case 'add_user':
		$user = new stdClass;
        $user = json_decode($_POST['user']);
        $validate = $users->validate($user);

        if($validate <> FALSE){

            $data = array(
                "status" => 0,
                "mensagem" => $validate
            );
            print json_encode($data);

        } else {

            $userID = new stdClass;
            $userID = json_decode($users->add($user));

            foreach ($user->grupos as $grupo){
                $data = new stdClass;
                $data->id = $userID->id;
                $data->grupo_ID = $grupo;
                $groups->add($data);
            }
            print json_encode($userID);

        }
	break;
	
	case 'delete_user':
		$user = new stdClass;
		$user = json_decode($_POST['user']);
		print $users->delete($user);		
	break;

    case 'edit_user':
        $user = new stdClass;
        $user = json_decode($_POST['user']);
        $validate = $users->validate($user);

        if($validate <> FALSE){

            $data = array(
                "status" => 0,
                "mensagem" => $validate
            );
            print json_encode($data);

        } else {

            $groups->delete($user);
            foreach ($user->grupos as $grupo){
                $data = new stdClass;
                $data->id = $user->id;
                $data->grupo_ID = $grupo;
                $groups->add($data);
            }
            print $users->edit($user);

        }
    break;

    default:
        print json_encode(array("status" => 0));
        return;
}

exit();